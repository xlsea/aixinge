package robot

import (
	"testing"
)

const token = "access token"
const secret = "加签密钥"

var dingTalkRobot = NewDingTalkRobot(token, secret)

func TestDingTalkSendMessage(t *testing.T) {
	msg := map[string]interface{}{
		"msgtype": "text",
		"text": map[string]string{
			"content": "这是一条AiXinGe钉钉消息测试.",
		},
		"at": map[string]interface{}{
			"atMobiles": []string{},
			"isAtAll":   false,
		},
	}

	if err := dingTalkRobot.SendMessage(msg); err != nil {
		t.Error(err)
	}
}

func TestDingTalkSendTextMessage(t *testing.T) {
	if err := dingTalkRobot.SendTextMessage("普通文本消息", []string{}, false); err != nil {
		t.Error(err)
	}
}

func TestDingTalkSendMarkdownMessage(t *testing.T) {
	err := dingTalkRobot.SendMarkdownMessage(
		"Markdown Test Title",
		"### Markdown 测试消息\n* 爱信鸽: [axg](https://gitee.com/aixinge)\n* AiXinGe\n ![](https://aixinge.com/images/logo.png)",
		[]string{},
		false,
	)
	if err != nil {
		t.Error(err)
	}
}

func TestDingTalkSendLinkMessage(t *testing.T) {
	err := dingTalkRobot.SendLinkMessage(
		"Link Test Title",
		"这是一条链接测试消息",
		"https://gitee.com/aixinge",
		"https://aixinge.com/images/logo.png",
	)

	if err != nil {
		t.Error(err)
	}
}
