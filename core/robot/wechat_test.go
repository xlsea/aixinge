package robot

import (
	"testing"
)

var weChatRobot = NewWechatRobot("机器人key")

func TestWechatSendMessage(t *testing.T) {
	msg := map[string]interface{}{
		"msgtype": "text",
		"text": map[string]interface{}{
			"content":               "这是一条AiXinGe企业微信消息测试.",
			"mentioned_list":        []string{},
			"mentioned_mobile_list": []string{},
		},
	}

	if err := weChatRobot.SendMessage(msg); err != nil {
		t.Error(err)
	}
}

func TestWechatSendMarkdownMessage(t *testing.T) {
	err := weChatRobot.SendMarkdownMessage("实时新增用户反馈<font color=\"warning\">132例</font>，请相关同事注意。\n" +
		">类型:<font color=\"comment\">用户反馈</font>" +
		">普通用户反馈:<font color=\"comment\">117例</font>" +
		">VIP用户反馈:<font color=\"comment\">15例</font>")
	if err != nil {
		t.Error(err)
	}
}

func TestWechatSendNewsMessage(t *testing.T) {
	err := weChatRobot.SendNewsMessage("图文消息", "信鸽推送图文消息",
		"https://gitee.com/aixinge", "https://aixinge.com/images/logo.png")
	if err != nil {
		t.Error(err)
	}
}
