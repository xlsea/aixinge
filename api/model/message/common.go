package message

// MsgType 消息类型
type MsgType int

const (
	Sms          MsgType = iota + 1 // 短信
	Mail                            // 邮件
	Inbox                           // 站内信
	WebSocket                       // WebSocket
	MiniPrograms                    // 小程序
)

func (m MsgType) IsValid() bool {
	return 0 < m && m < MiniPrograms
}

// ChannelProvider 消息服务商
type ChannelProvider int

const (
	AliCloud     ChannelProvider = iota + 1 // 阿里云
	TencentCloud                            // 腾讯云
	BaiduCloud                              // 百度云
	HuaweiCloud                             // 华为云
)

func (c ChannelProvider) IsValid() bool {
	return 0 < c && c < HuaweiCloud
}
