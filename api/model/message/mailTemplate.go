package message

import (
	"aixinge/api/model/common"
	"aixinge/global"
	"aixinge/utils/snowflake"
)

type MailTemplate struct {
	global.MODEL
	AppId       snowflake.ID       `json:"appId,omitempty" swaggertype:"string"` // 应用 ID
	Name        string             `json:"name"`                                 // 名称
	Subject     string             `json:"subject"`                              // 主题
	Content     string             `json:"content"`                              // 内容
	Type        int                `json:"type"`                                 // 类型（1-文本、2-HTML）
	Attachments common.Attachments `json:"attachments" gorm:"type:json"`         // 附件 JSON
	Remark      string             `json:"remark"`                               // 备注
	Status      int                `json:"status"`                               // 状态 1、正常 2、禁用
}
