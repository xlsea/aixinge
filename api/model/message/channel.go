package message

import (
	"aixinge/api/model/common"
	"aixinge/global"
)

type Channel struct {
	global.MODEL
	Name     string           `json:"name"`                    // 消息渠道名称
	Type     MsgType          `json:"type"`                    // 消息类型(枚举)
	Provider ChannelProvider  `json:"provider"`                // 消息服务提供商
	Weight   int              `json:"weight"`                  // 权重
	Config   common.MapConfig `json:"config" gorm:"type:json"` // 消息渠道配置（JSON）
	Remark   string           `json:"remark"`                  // 备注
	Status   int              `json:"status"`                  // 状态 1、正常 2、禁用
}

func (c *Channel) ConfigString(key string) (result string) {
	value := c.Config[key]
	if value != nil {
		result = value.(string)
	}
	return result
}
