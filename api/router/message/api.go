package message

import (
	v1 "aixinge/api/v1"
	"github.com/gofiber/fiber/v2"
)

type ApiRouter struct {
}

func (a *ApiRouter) InitApiRouter(router fiber.Router) {
	apiRouter := router.Group("api")
	var api = v1.AppApi.MessageApi.Api
	{
		apiRouter.Post("create", api.Create) // 创建
	}
}
