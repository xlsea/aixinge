package message

type RouterGroup struct {
	ApiRouter
	ApplicationRouter
	ChannelRouter
	ChannelTemplateRouter
	MailLogRouter
	MailTemplateRouter
}
