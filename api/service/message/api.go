package message

import (
	"aixinge/api/model/message"
	messageReq "aixinge/api/model/message/request"
	"errors"
)

type ApiService struct {
}

var messageService ServiceGroup

func (m *ApiService) Create(params messageReq.ApiParams) error {
	if !params.Type.IsValid() {
		return errors.New("消息类型不合法")
	}

	channelList := messageService.ChannelService.ListByTypeAndTemplateId(params.Type, params.TemplateId)
	if len(channelList) < 1 {
		return errors.New("指定模板消息发送渠道不存在")
	}

	var err error
	switch params.Type {
	case message.Sms:
		// 短信
	case message.Mail:
		// 邮件
		err = messageService.MailTemplateService.SendMail(params, channelList)
	}
	return err
}
