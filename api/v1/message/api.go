package message

import (
	"aixinge/api/model/common/response"
	messageReq "aixinge/api/model/message/request"
	"aixinge/api/model/validation"
	"aixinge/global"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type Api struct {
}

// Create
// @Tags Api
// @Summary 创建消息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body messageReq.ApiParams true "创建消息"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建消息成功"}"
// @Router /v1/api/create [post]
func (m *Api) Create(c *fiber.Ctx) error {
	var params messageReq.ApiParams
	_ = c.BodyParser(&params)
	if err := validation.Verify(params, validation.ApiParams); err != nil {
		return response.FailWithMessage(err.Error(), c)
	}
	err := apiService.Create(params)
	if err != nil {
		global.LOG.Error("创建消息失败！", zap.Any("err", err))
		return response.FailWithMessage("创建消息失败："+err.Error(), c)
	}
	return response.OkWithMessage("创建消息成功！", c)
}
